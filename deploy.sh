#!/bin/sh

usage () {
    {
        echo "Usage: $0 [--vagrant]"
        echo "Usage: $0 [--vagrant] base"
        echo "Usage: $0 [--vagrant] users"
        echo "Usage: $0 [--vagrant] services [SERVICE]"
    } >&2
}

BASE_CMD="ansible-playbook playbook.yml"

if [ "$1" = "--vagrant" ]; then
    BASE_CMD="$BASE_CMD --verbose --inventory=vagrant_host"
    VAGRANT_VAR="from_vagrant"
    shift
fi

if [ -z "$(ansible-galaxy collection list community.general 2>/dev/null)" ]; then
    echo "Installing community.general modules"
    ansible-galaxy collection install community.general
fi

if [ -z "$1" ]; then
    echo "Deploying all!"
    $BASE_CMD
else
    case $1 in
        "services")
            if [ -z "$2" ]; then
                echo "Deploying all services!"
                eval "$BASE_CMD --tags setup_services $(test -z "$VAGRANT_VAR" || printf '%s' "$VAGRANT_VAR=true")"
            else
                echo "Deploying service: $2"
                $BASE_CMD --tags setup_services --extra-vars '{"single_service": "'"$2"'"'"$(test -z "$VAGRANT_VAR" || printf '%s' ', "'"$VAGRANT_VAR"'": true')"'}'
            fi
        ;;
        "base")
            eval "$BASE_CMD --tags base_only $(test -z "$VAGRANT_VAR" || printf '%s' "$VAGRANT_VAR=true")"
        ;;
        "users")
            eval "$BASE_CMD --tags setup-users $(test -z "$VAGRANT_VAR" || printf '%s' "$VAGRANT_VAR=true")"
        ;;
        *)
            usage
            exit 1
        ;;
    esac
fi

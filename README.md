# data.coop infrastructure

This repository contains the code used to deploy data.coop's services
and websites. We use Ansible to encode our infrastructure setup. Only
the association's administrators have access to deploy the services.

## Deploying

To deploy the services, the included `deploy.sh` script can be used. The
Ansible playbook uses two custom-made roles (in the `roles/` directory):

- `ubuntu_base` - used to configure the host itself and install the
  necessary packages
- `docker` - used to deploy our services and websites with Docker
  containers

The script has options to deploy only one of the roles. Select services
only can also be specified. By default, the script deploys everything.

Here is a summary of the options that can be used with the script:

```sh
# deploy everything
./deploy.sh

# deploy the ubuntu_base role only
./deploy.sh base

# deploy user setup only
./deploy.sh users

# deploy the docker role only
./deploy.sh services

# deploy SINGLE_SERVICE Docker service only
./deploy.sh services SINGLE_SERVICE
```

`SINGLE_SERVICE` should match one of the service names in the `services`
dictionary in `roles/docker/defaults/main.yml` (e.g. `gitea` or
`data_coop_website`).

## Testing

In order for us to be able to test our setup locally, we use Vagrant to
deploy the services in a virtual machine. To do this, Vagrant and
VirtualBox must both be installed on the development machine. Then, the
services can be deployed locally by using the `vagrant` command-line
tool. The working directory needs to be the root of the repository for
this to work properly.

> Note: As our secrets are contained in an Ansible Vault file, only the
> administrators have the ability to run the deployment in Vagrant.
> However, one could replace the vault file for testing purposes.

Here is a summary of the commands that are available with the `vagrant`
command-line tool:

```sh
# Create and provision the VM
vagrant up

# Re-provision the VM
vagrant provision

# SSH into the VM
vagrant ssh

# Power down the VM
vagrant halt

# Power down and delete the VM
vagrant destroy
```

The `vagrant` command-line tool does not support supplying extra
variables to Ansible on runtime, so to be able to deploy only parts of
the Ansible playbook to Vagrant, the `deploy.sh` script can be used with
the `--vagrant` flag. Here are some examples:

```sh
# deploy the ubuntu_base role only in the Vagrant VM
./deploy.sh --vagrant base

# deploy SINGLE_SERVICE Docker service only in the Vagrant VM
./deploy.sh --vagrant services SINGLE_SERVICE
```

Note that the `--vagrant` flag should be the first argument when using
the script.

## Contributing

If you want to contribute, you can fork the repository and submit a pull
request. We use a pre-commit hook for linting the YAML files before
every commit, so please use that. To initialize pre-commit, you need to
have Python and GNU make installed. Then, just run the following shell
command:

```sh
make init
```

## Nice tools

- [J2Live](https://j2live.ttl255.com/): A live Jinja2 parser, nice to
  test out filters


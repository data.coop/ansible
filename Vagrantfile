Vagrant.require_version ">= 2.0.0"
PORT = 19022

def provisioned?(vm="default", provider="virtualbox")
  File.exist?(".vagrant/machines/#{vm}/#{provider}/action_provision")
end

Vagrant.configure(2) do |config|
  config.vm.network :private_network, ip: "192.168.56.10"
  config.vm.network :forwarded_port, guest: PORT, host: PORT

  config.vm.box = "ubuntu/focal64"
  config.vm.hostname = "datacoop"

  config.vm.provider :virtualbox do |v|
    v.cpus = 8
    v.memory = 16384
  end

  config.vm.provision :ansible do |ansible|
    ansible.compatibility_mode = "2.0"
    ansible.playbook = "playbook.yml"
    ansible.ask_vault_pass = true
    ansible.verbose = "v"

    # If the VM is already provisioned, we need to use the new port
    if provisioned?
      config.ssh.guest_port = PORT
      ansible.extra_vars = {
        ansible_port: PORT,
        from_vagrant: true
      }
    else
      ansible.extra_vars = {
        from_vagrant: true
      }
    end
  end
end

init: create_venv install_pre_commit install_ansible_galaxy_modules

create_venv:
	python3 -m venv venv
	venv/bin/pip install -U pip
	venv/bin/pip install ansible pre-commit

install_pre_commit:
	venv/bin/pre-commit install

install_ansible_galaxy_modules:
	venv/bin/ansible-galaxy collection install community.general